import React from 'react';

import { BrowserRouter, Routes, Route } from 'react-router-dom';
import AsyncComponent from './components/AsyncComponent';
import React16 from './pages/React16';
import { Animation } from './pages';

// import About and Home components asynchronously
const About = AsyncComponent(() => import('./pages/About'));
const Home = AsyncComponent(() => import('./pages/Home'));

const App = () => (
  <BrowserRouter>
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/about" element={<About />} />
      <Route path="/react16" element={<React16 />} />
      <Route path="/animation" element={<Animation />} />
    </Routes>
  </BrowserRouter>
);

export default App;
