import React, { useState } from 'react';
import { v4 as uuidv4 } from 'uuid';

import Portal from './Portal';
import { Button } from '../StyledComponents';

const PortalSection = () => {
  const [inputText, setInputText] = useState('');
  const [notifs, setNotifs] = useState([]);

  const renderNotifs = notifs.map((notif) => (
    <p key={notif.key}>{notif.message}</p>
  ));

  const onSubmitNotif = (e) => {
    e.preventDefault();

    const newNotif = {
      message: inputText,
      key: uuidv4(),
    };

    setNotifs([...notifs, newNotif]);

    setTimeout(() => {
      setNotifs(notifs.filter((notif) => notif.key !== newNotif.key));
    }, 2000);
  };

  const onInputChange = (e) => {
    e.preventDefault();
    setInputText(e.target.value);
  };

  const clearNotifs = (e) => {
    e.preventDefault();
    setNotifs([]);
  };

  return (
    <div>
      <h2>Portal Example</h2>
      <form action="submit">
        <input type="text" value={inputText} onChange={onInputChange} />
        <Button type="submit" onClick={onSubmitNotif}>
          Send Notification
        </Button>
        <Button onClick={clearNotifs}>Clear Notifications</Button>
      </form>
      <Portal>{renderNotifs}</Portal>
    </div>
  );
};

export default PortalSection;
