import styled from 'styled-components';

export const Button = styled.button`
  box-sizing: border-box;
  display: block;
  background-color: #21d212;
  padding: 6px 4px;
  border: 0;
  box-shadow: 2px 2px 2px rgba(0, 0, 0, 0.2);
  font-size: 16px;
  cursor: pointer;

  &:active {
    position: relative;
    top: 2px;
    background-color: #21c200;
  }
`;
