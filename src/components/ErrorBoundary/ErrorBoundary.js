import React from 'react';

class ErrorBoundary extends React.Component {
  state = { hasError: false, errorMessage: null };

  componentDidCatch(error, info) {
    // display some fallback UI
    console.log(error);
    this.setState({ hasError: true, errorMessage: error.message });

    // maybe log the error, etc.
  }

  render() {
    if (this.state.hasError) {
      return (
        <h2>The following error was thrown: {this.state.errorMessage}.</h2>
      );
    }
    return this.props.children;
  }
}

export default ErrorBoundary;
