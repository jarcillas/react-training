import React from 'react';
import { Button } from '../StyledComponents';

class ErrorBoundarySection extends React.Component {
  state = { throw: false };

  onClick = () => {
    this.setState({ throw: true });
  };

  render() {
    if (this.state.throw) throw new Error('Whooops!');

    return (
      <div>
        <h2>Error Boundary</h2>
        <Button onClick={this.onClick}>Click me!</Button>
      </div>
    );
  }
}

export default ErrorBoundarySection;
