import React from 'react';
import { NavLink } from 'react-router-dom';
import Login from '../Login';

const Navigation = () => (
  <nav id="nav">
    <div id="nav__left">
      <h1 id="nav__title">AMAGI Bike Shop</h1>
      <div id="nav__links">
        <NavLink to="/">Home</NavLink>
        <NavLink to="/about">About</NavLink>
        <NavLink to="/react16">React 16</NavLink>
        <NavLink to="/animation">Animation</NavLink>
      </div>
    </div>
    <Login />
  </nav>
);

export default Navigation;
