import React, { useState } from 'react';

// const Login = () => {
//   [user, setUser] = useState('');
//   [loggedIn, setLoggedIn ] = useState(false);
// }

class Login extends React.Component {
  state = {
    user: '',
    loggedIn: false,
  };

  setUser = (user) => {
    this.setState({ user });
  };

  login = (e) => {
    e.preventDefault();
    this.setState({ loggedIn: true });
  };

  logout = () => {
    this.setState({ loggedIn: false, user: '' });
  };

  render() {
    const { user, loggedIn } = this.state;

    return (
      <div id="nav__login">
        {!loggedIn && (
          <>
            <div id="nav__login__label">
              Please enter your username and click Login.
            </div>
            <form id="login-form" action="submit">
              <input
                type="text"
                value={user}
                onChange={(e) => this.setUser(e.target.value)}
                placeholder="username"
              />
              <input
                className="login-logout-button"
                type="submit"
                disabled={!user}
                onClick={this.login}
                value="Login"
              />
            </form>
          </>
        )}
        {loggedIn && (
          <>
            <div id="nav__login__label">
              Welcome to your dashboard, <b>{user}</b>!
            </div>
            <button className="login-logout-button" onClick={this.logout}>
              Logout
            </button>
          </>
        )}
      </div>
    );
  }
}

export default Login;
