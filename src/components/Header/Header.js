import React from 'react';

const Header = () => (
  <header id="header">
    <h2>Welcome to My Bike Shop!</h2>
    <p>Need a ride? Go inside and we'll get you one.</p>
    <a target="_blank" rel="noopener noreferrer" href="https://www.google.com">
      Buy one here!
    </a>
  </header>
);

export default Header;
