import React from 'react';
import { CSSTransition, TransitionGroup } from 'react-transition-group';

import { Header, Navigation } from '../../components';

import './Animation.css';

class Animation extends React.Component {
  state = {
    pokemon: ['Charmander', 'Pikachu', 'Bulbasaur', 'Squirtle'],
  };

  handleAdd = () => {
    const newPokemon = this.state.pokemon.concat([prompt('Enter a name')]);
    this.setState({ pokemon: newPokemon });
  };

  handleRemove = (i) => {
    let newPokemon = this.state.pokemon.slice();
    newPokemon.splice(i, 1);
    this.setState({ pokemon: newPokemon });
  };

  render() {
    const { pokemon } = this.state;
    return (
      <div>
        <Navigation />
        <Header />
        <h3>Pokemon Names</h3>
        <ol>
          <TransitionGroup>
            {pokemon.map((poke, i) => (
              <CSSTransition
                key={poke}
                classNames="animate"
                timeout={{ appear: 1000, enter: 500, exit: 300 }}
                e
              >
                <li>
                  {poke}{' '}
                  <span
                    className="remove-icon"
                    onClick={() => this.handleRemove(i)}
                  >
                    X
                  </span>
                </li>
              </CSSTransition>
            ))}
          </TransitionGroup>
        </ol>
        <button className="add-button" onClick={this.handleAdd}>
          Add Name
        </button>
      </div>
    );
  }
}

export default Animation;
