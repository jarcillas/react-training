import React from 'react';
import { Header, Navigation } from '../../components';

const Home = () => (
  <div>
    <Navigation />
    <Header />
    <div className="page">Home</div>
  </div>
);

export default Home;
