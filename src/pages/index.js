import About from './About';
import Home from './Home';
import Animation from './Animation';

export { About, Home, Animation };
