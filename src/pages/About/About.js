import React from 'react';
import { Header, Navigation } from '../../components';

const About = () => (
  <div>
    <Navigation />
    <Header />
    <div className="page">About Page</div>
  </div>
);

export default About;
