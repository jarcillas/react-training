import React from 'react';

import { ErrorBoundary, Header, Navigation } from '../components';
import ErrorBoundarySection from '../components/ErrorBoundary/ErrorBoundarySection';
import PortalSection from '../components/Portal/PortalSection';

const React16 = () => {
  return (
    <div>
      <Navigation />
      <Header />
      <ErrorBoundary>
        <ErrorBoundarySection />
      </ErrorBoundary>
      <PortalSection />
    </div>
  );
};

export default React16;
